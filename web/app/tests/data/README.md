# Test data

This directory contains test data using to seed the farm and snapshot database.

## Archives data

Archives data are located under the `archives/` directory. Each sub-directory is
a single archive (such as `debian`, `debian-security`, `debian-backports` and so
on).

For each archive, there are a directory name initial which contains the files
that will be used on the first import run. Each subsequant runs will use the
patches files numbered as run-NN.patch. Patches are applied sequancially.

## Scripts and templates

Some scripts and templates are available in order to facilitate generating new
tests samples. You will find them under `scripts/` and `templates/`.
