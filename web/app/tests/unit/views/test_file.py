# snapshot.debian.org - web frontend
# https://salsa.debian.org/snapshot-team/snapshot
#
# Copyright (c) 2021 Baptiste Beauplat <lyknode@cilg.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from html import unescape
from pytest import mark


@mark.parametrize('path,error', (
    ('', 'The requested URL was not found'),
    ('/', 'The requested URL was not found'),
    ('/not-a-digest', 'Invalid hash format'),
    ('/0000000000000000000000000000000000000000',
     'Ooops, we do not have a file'),
))
def test_file_bad(client, path, error):
    response = client.get(f'/file{path}')

    assert response.status_code == 404
    assert error in \
        unescape(response.data.decode())


@mark.parametrize('content,access,code,message,filename', (
    ('file content', True, 200, 'file content', ''),
    ('file content', True, 200, 'file content', 'file.txt'),
    ('another content', False, 403, 'Ooops, cannot read file with digest', '')
))
def test_file(client, file_ctrl, content, access, code, message, filename):
    digest = file_ctrl.create_file(content, access)
    file_path = f'/{filename}' if filename else ''
    response = client.get(f'/file/{digest}{file_path}')

    assert response.status_code == code
    assert message in response.data.decode()
    if filename:
        # the file controller does not register the file in the DB,
        # hence the file is just served as-is
        filename = digest
        assert f'filename={filename}' in \
            response.headers['Content-Disposition']

    file_ctrl.cleanup_file(digest)
