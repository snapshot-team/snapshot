# snapshot.debian.org - web frontend
# https://salsa.debian.org/snapshot-team/snapshot
#
# Copyright (c) 2020 Baptiste Beauplat <lyknode@cilg.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from shutil import copytree, rmtree, copy
from os.path import join, dirname, abspath, isdir, isfile, basename
from glob import glob
from os import replace
from tempfile import mkdtemp
from logging import getLogger
from subprocess import Popen, PIPE, STDOUT

log = getLogger(__name__)


class PackageController():
    def __init__(self, dataset):
        self.dataset = dataset
        self.cache = mkdtemp(prefix='snapshot-package-cache-')
        self.packages = []

        self._build_packages()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        if isdir(self.cache):
            rmtree(self.cache)

    def _build_packages(self):
        to_build = self._extract_build_list()

        for package, versions in to_build.items():
            for version in versions:
                self.packages.append(self._build_package(package, version))

    def _build_package(self, package, version):
        log.warning(f'Building package: {package}-{version}')

        with PackageSource(package, version) as source:
            files = self._install_to_cache(source.get_package_files())

        return {
            'package': package,
            'version': version,
            'files': files,
        }

    def _install_to_cache(self, files):
        installed = []

        for filename in files:
            copy(filename, self.cache)
            installed.append(join(self.cache, basename(filename)))

        return installed

    def _extract_build_list(self):
        to_build = {}

        for run in self.dataset.values():
            for archive in run.values():
                for distrib in archive.values():
                    for package, version in distrib.items():
                        if package in to_build:
                            to_build[package].add(version)
                        else:
                            to_build[package] = set({version})

        return to_build

    def get_changes(self, name, version):
        files = [
            package['files'] for package in self.packages if
            package['package'] == name and package['version'] == version
        ][0]

        return [
            filename for filename in files if filename.endswith('.changes')
        ][0]


class PackageSource():
    template = join(dirname(abspath(__file__)), '..', 'data', 'templates',
                    'package')

    def __init__(self, package, version):
        self.work_dir = mkdtemp(prefix='snapshot-package-')
        self.source_dir = join(self.work_dir, f'{package}-{version}')
        self.build_dir = join(self.work_dir, 'build')
        self.package = package
        self.version = version

        self._customize_template()
        self._build_package()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        if isdir(self.work_dir):
            rmtree(self.work_dir)

    def _customize_template(self):
        copytree(self.template, self.source_dir)

        for filename in ('control', 'changelog',):
            old_file = join(self.source_dir, 'debian', filename)
            with open(f'{old_file}.new', 'w') as new_file:
                with open(old_file) as fh:
                    for line in fh:
                        new_file.write(
                            line.replace('PACKAGE', self.package).
                            replace('VERSION', self.version)
                        )
            replace(f'{old_file}.new', old_file)

    def _run_command(self, command, args, workdir):
        proc = Popen([command] + args,
                     stdout=PIPE,
                     stderr=STDOUT,
                     cwd=join(workdir))
        (output, _) = proc.communicate()

        if proc.returncode != 0:
            raise Exception('command failed:\n'
                            f'command: {command}\n'
                            f'args: {args}\n'
                            f'workdir: {workdir}\n\n'
                            f'output: {output.decode()}')

        return output.strip()

    def _build_package(self):
        args = ['--build=full',
                '--no-check-builddeps',
                '--sign-key=434F639E8880A43556CACA359ACF4AF8EF70E16E',
                '--sign-command=gpg',
                '--sign-backend=gpg',
                '--force-sign']
        command = 'dpkg-buildpackage'

        self._run_command(command, args, self.source_dir)

    def get_package_files(self):
        return [filename for filename in glob(join(self.work_dir, '*')) if
                isfile(filename)]
