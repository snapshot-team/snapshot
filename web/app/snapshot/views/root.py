# snapshot.debian.org - web frontend
#
# Copyright (c) 2009, 2010 Peter Palfrader
# Copyright (c) 2020 Baptiste Beauplat <lyknode@cilg.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from logging import getLogger
from urllib.parse import quote

from flask import Blueprint, render_template, request, current_app

from snapshot.lib.control_helpers import link_quote_array, get_domain
from snapshot.lib.cache import cache
from snapshot.models.snapshot import get_snapshot_model

log = getLogger(__name__)
router = Blueprint("root", __name__, url_prefix="")


@router.route("/")
@cache()
def index():
    index.cache_timeout = current_app.config['CACHE_TIMEOUT_ROOT']
    snapshot_model = get_snapshot_model()

    names = link_quote_array(snapshot_model.archives_get_list())
    srcstarts = link_quote_array(snapshot_model.packages_get_name_starts())
    binstarts = link_quote_array(snapshot_model.packages_get_name_starts(
        get_binary=True))

    return render_template('root/root.html', names=names, srcstarts=srcstarts,
                           binstarts=binstarts)


@router.route("/oldnews")
@cache()
def oldnews():
    oldnews.cache_timeout = current_app.config['CACHE_TIMEOUT_ROOT_NEWS']
    breadcrumbs = _build_crumbs('older news')

    return render_template('root/misc-oldnews.html', breadcrumbs=breadcrumbs)


def _build_crumbs(page=None):
    crumbs = []

    url = quote(request.environ.get('SCRIPT_NAME')) + "/"
    crumbs.append({'url': url, 'name': get_domain()})
    crumbs.append({'url': None, 'name': page, 'sep': ''})

    return crumbs
