#!/usr/bin/python3
#
# Copyright (c) 2024 Philipp Kern
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

def upgrade(db):
    db.execute("""
        CREATE TABLE node_with_ts_materialized AS
          SELECT node.node_id,
            node.parent,
            node.first,
            node.last,
            mirrorrun_first.run AS first_run,
            mirrorrun_first.archive_id,
            mirrorrun_last.run AS last_run
          FROM node
            JOIN mirrorrun mirrorrun_first ON mirrorrun_first.mirrorrun_id = node.first
            JOIN mirrorrun mirrorrun_last ON mirrorrun_last.mirrorrun_id = node.last;
        CREATE UNIQUE INDEX node_with_ts_materialized_node_id
          ON node_with_ts_materialized USING btree (node_id);
        CREATE INDEX node_with_ts_materialized_parent
          ON node_with_ts_materialized USING btree (parent);
        """)

    db.execute("UPDATE config SET value='26' WHERE name='db_revision' AND value='25'")

# vim:set et:
# vim:set ts=4:
# vim:set shiftwidth=4:
